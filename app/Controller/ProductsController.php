<?php
App::uses('AppController', 'Controller');
/**
 * Products Controller
 *
 * @property Product $Product
 */

class ProductsController extends AppController {

    public function index() {
        $json_url = "http://mcafee.0x10.info/api/app?type=json";
        $json = file_get_contents($json_url);
        $jsondataarray = json_decode($json, true);
        $this->set(compact('jsondataarray'));
        $this->set('products', $this->Product->find('all'));
    }

    public function add() {
        $this->loadModel('Product');
        if ($this->request->is('post')) {
            $this->Product->create();
            if ($this->Product->save($this->request->data)) {
                $this->Session->setFlash(__('Product has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Product could not be saved. Please, try again.'));
            }
        }
    }

    public function portfolio() {
    }
}