<?php foreach ($jsondataarray as $element): ?>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
        <!-- ITEM -->
        <div class="panel price panel-blue">
            <div class="panel-heading arrow_box text-center">
                <h5><?php echo h($element['name']); ?></h5>
            </div>
            <div class="panel-body text-center">
                <img src="<?php echo $element['imagee']?>" height="100" width="100">
            </div>
            <ul class="list-group list-group-flush text-center">
                <li class="list-group-item"><i class="icon-ok text-info"></i>Rating: <?php echo $element['rating']?></li>
                <li class="list-group-item"><i class="icon-ok text-info"></i>Type: <?php echo $element['type']?></li>
                <li class="list-group-item"><i class="icon-ok text-info"></i>Last Updated: <?php echo $element['last_update']?></li>
            </ul>
            <div class="panel-footer">
                <a class="btn btn-lg btn-block btn-info" href="<?php echo $element['url']?>" target="_blank">
                    <?php
                        if($element['price'] == 0) {
                            echo 'FREE';
                        } else {
                            echo '$ '.$element['price'];
                        }
                    ?>
                </a>
            </div>
        </div>
        <!-- / ITEM -->
    </div>
<?php endforeach; ?>