<div class="panel panel-default">
    <div class="panel-heading">
        <!-- Panel Header -->
        <b>Portfolio Page</b>
        <!-- / Panel Header -->
    </div>
    <div class="panel-body">
        <!-- Panel Body -->
        <div class="col-md-12">
            <ul>
                <li>
                    <h4>JobsIIT: Online Jobs Portal &nbsp;&nbsp; <a href="http://jobsiit.com" target="_blank">Link to site</a></h4>
                    <p></p>
                    <p>
                        JobsIIT is an exclusive online job portal for students and alumni of elite engineering
                        colleges.<br>
                        Contributed in Front end and Back end development in JavaScript, CakePHP and Bootstrap. Also, designed Data
                        Model for the portal.
                    </p>
                </li>

                <li>
                    <h4>National Rural Employment Program</h4>
                    <p></p>
                    <p>
                        NREG Program is an Enterprise portal to automate the system and aid the government<br>
                        Portal was developed using JSF, EJB and JPA frameworks of J2EE.
                        Biometric Authentication using Socket Programming and C language was implemented to manage
                        attendance of consumers.
                    </p>
                </li>
            </ul>
            <br>
            <h3>
                Author: Sanjay Kumar<span class="pull-right">Contact: +918722544411</span>
            </h3>

        </div>
        <!-- / Panel Body -->
    </div>
</div>