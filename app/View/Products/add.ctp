<div class="panel panel-default">
    <div class="panel-heading">
        <!-- Panel Header -->
        <b>Add New Product</b>
        <!-- / Panel Header -->
    </div>
    <div class="panel-body">
        <div class="col-md-12">
            <?php echo $this->Form->create('Product'); ?>
            <div class="col-md-5">
                <div class="form-group">
                    <?php echo $this->Form->input('name', array('label' => 'Product Name<span class="mandatory">*</span>', 'class'=>'form-control', 'placeholder'=>'Ex: McAfee Antivirus'));?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->input('imagee', array('label' => 'Product Image URL<span class="mandatory">*</span>', 'class'=>'form-control', 'placeholder'=>'Ex: mcafee.com/image'));?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->input('type', array('label' => 'Product Type<span class="mandatory">*</span>', 'class'=>'form-control', 'placeholder'=>'Ex: Productivity'));?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->input('price', array('label' => 'Product Price<span class="mandatory">*</span>', 'class'=>'form-control', 'placeholder'=>'Ex: 4.5'));?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->input('rating', array('label' => 'Product Rating<span class="mandatory">*</span>', 'class'=>'form-control', 'options'=>$rating));?>
                </div>
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <?php echo $this->Form->input('users', array('label' => 'Product Users<span class="mandatory">*</span>','class'=>'form-control','placeholder'=>'Ex: 2000'));?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->input('last_update', array('label' => 'Product Last Updated<span class="mandatory">*</span>', 'class'=>'form-control', 'placeholder'=>'Ex: 25, Apr 2015'));?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->input('description', array('label' => 'Product Description<span class="mandatory">*</span>', 'class'=>'form-control', 'placeholder'=>'Ex: Product Description'));?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->input('url', array('label' => 'Product URL<span class="mandatory">*</span>', 'class'=>'form-control', 'placeholder'=>'Ex: play.google.com/product'));?>
                </div>
                <br>
                <?php echo $this->Form->end(array('label'=>'Create', 'class'=>'btn btn-success pull-right', 'id'=>'addProduct')); ?>
            </div>
        </div>
    </div>
</div>